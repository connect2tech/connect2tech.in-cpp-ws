
#include <iostream>
using namespace std;

/*
class MyAccount {
public:
    virtual void withdraw(double amount) {
        std::cout << "MyAccount/withdraw" << std::endl;
    }
};


class SavingAccount: public MyAccount  {
public:
    void withdraw(double amount) {
        std::cout << "SavingAccount/withdraw" << std::endl;
    }
};

int main() {
    MyAccount *acc1 = new MyAccount();
    MyAccount *acc2 = new SavingAccount();
    
    //acc1->withdraw(1000);
    //acc2->withdraw(1000);
    
    SavingAccount sa;
    sa.withdraw(2000);
    

    delete acc1;
    delete acc2;
        
    return 0;
}
*/

//------------------------------------------

/*
class MyAccount {
public:
    void display(){
        cout<<"MyAccount"<<endl;
    }
};


class SavingAccount: public MyAccount  {
public:
   void display(){
        cout<<"SavingAccount"<<endl;
    }

};

void display_account(const MyAccount &acc){
        acc.display();
}

int main() {
    
    MyAccount a;
    SavingAccount b;
    display_account(a);
    //display_account(b);
    
    return 0;
}
 * */

//------------------------------------------
/*
class Shape{
  public:
    virtual int Area() = 0;
};

class Rectangle:public Shape{
 public:   
    int Area(){
        cout<<"Area"<<endl;
    }
};


int main() {
    
    Rectangle r;
    r.Area();
    
    return 0;
}*/