#include <iostream>
using namespace std;

/*
//Single Inheritance
// base class
class Vehicle
{
public:
    Vehicle()
    {
        cout << "This is a Vehicle" << endl;
    }
    
    void displayVehicle(){
        cout << "This is displayVehicle" << endl;
    }
};

// sub class derived from two base classes
class Car : public Vehicle
{
public:
    Car()
    {
        cout << "This is a Car" << endl;
    }
    
    void displayCar(){
        cout << "This is displayCar" << endl;
    }
};

// main function
int main()
{
    // creating object of sub class will
    // invoke the constructor of base classes
    Car obj;
    obj.displayCar();
    obj.displayVehicle();
    return 0;
}
*/

//-------------------------------------------------------------------

/*
//Multiple Inheritance
// first base class 
class Vehicle { 
  public: 
    Vehicle() 
    { 
      cout << "This is a Vehicle" << endl; 
    } 
}; 
  
// second base class 
class FourWheeler { 
  public: 
    FourWheeler() 
    { 
      cout << "This is a 4 wheeler Vehicle" << endl; 
    } 
}; 
  
// sub class derived from two base classes 
class Car: public Vehicle, public FourWheeler { 
  
}; 
  
// main function 
int main() 
{    
    // creating object of sub class will 
    // invoke the constructor of base classes 
    Car obj; 
    return 0; 
} 
*/

//-------------------------------------------------------------------

/*
// C++ program to implement  
// Hierarchical Inheritance 
#include <iostream> 
using namespace std; 
  
// base class 
class Vehicle  
{ 
  public: 
    Vehicle() 
    { 
      cout << "This is a Vehicle" << endl; 
    } 
}; 
  
  
// first sub class  
class Car: public Vehicle 
{ 
  
}; 
  
// second sub class 
class Bus: public Vehicle 
{ 
      
}; 
  
// main function 
int main() 
{    
    // creating object of sub class will 
    // invoke the constructor of base class 
    Car obj1; 
    Bus obj2; 
    return 0; 
} 
*/

//-------------------------------------------------------------------------

/*
// C++ program to implement  
// Multilevel Inheritance 
#include <iostream> 
using namespace std; 
  
// base class 
class Vehicle  
{ 
  public: 
    Vehicle() 
    { 
      cout << "This is a Vehicle" << endl; 
    } 
}; 
class fourWheeler: public Vehicle 
{  public: 
    fourWheeler() 
    { 
      cout<<"Objects with 4 wheels are vehicles"<<endl; 
    } 
}; 
// sub class derived from two base classes 
class Car: public fourWheeler{ 
   public: 
     car() 
     { 
       cout<<"Car has 4 Wheels"<<endl; 
     } 
}; 
  
// main function 
int main() 
{    
    //creating object of sub class will 
    //invoke the constructor of base classes 
    Car obj; 
    return 0; 
} 
*/

//-------------------------------------------------------------------------

//base class 
class Vehicle 
{ 
    public: 
    Vehicle() 
    { 
        cout<<"Fare of Vehicle\n"; 
    } 
}; 
  
// first sub class  
class Car: public Vehicle 
{ 
  
}; 
  
// second sub class 
class Bus: public Vehicle
{ 
      
}; 

class Fare: public Car, public Bus{
	
};
  
// main function 
int main() 
{    
    // creating object of sub class will 
    // invoke the constructor of base class 
    Fare obj2; 
    return 0; 
} 
