#include <iostream>
using namespace std;

/*
//Function to find out the maximum of 2 numbers
int max(int x, int y){
    int temp;

    if (x>y){
        temp = x;
    }else{
        temp = y;
    }

    return temp;
}

int main()
{
    max(10,20);
        cout("hello world\n");
        return 0;
}
*/

//---------------------------------------------------------

class Employee
{

    int empId;
    std::string name;

    Employee()
    {
        empId = 10;
        name = "CPP";
    }

    void details()
    {
        cout << empId << endl;
        cout << name << endl;
    }
};

int main()
{
    Employee e;
    return 0;
}