#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

/*
class Employee{

    int empId;
    std::string name;

    void details(){
        cout<<empId<<endl;
        cout<<name<<endl;
    }

};

int main()
{
        printf("hello world\n");
        return 0;
}
*/

//---------------------------------------------

/*

class AccessModifiers{
public:
    int a_public{10};
    
    void displayValues(){
        cout<<"Protected=>"<<a_protected;
    }
protected:
    int a_protected{20};
private:
    int a_private{20};
    
    
};

int main()
{
    AccessModifiers modifier;
        cout<<modifier.a_public<<endl;
    //cout<<modifier.a_protected<<endl;
    //cout<<modifier.a_private<<endl;
    modifier.displayValues();

        return 0;
}
*/

//---------------------------------------------

/*
class AccessModifiersBase
{
public:
    int a_public{ 10 };

protected:
    int a_protected{ 20 };

private:
    int a_private{ 20 };
};

class Derived_Public: public AccessModifiersBase{
    
public:
    void displayBase(){
        cout<<"public=>"<<a_public<<endl;
        cout<<"a_protected=>"<<a_protected<<endl;
    }
    
};

int main()
{
    Derived_Public derived1;
    //base.displayBase();
    cout<<derived1.a_public<<endl;
    //cout<<base.a_protected<<endl;

    return 0;
}*/
