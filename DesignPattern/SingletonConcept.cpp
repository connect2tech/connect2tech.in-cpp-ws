#include <iostream>
using namespace std;

class Singleton{

private:
    static Singleton *singleton;
    Singleton(){
    }
    
public:
    string value;
    static Singleton* getSingleton(){
        if(singleton == NULL){
            cout<<"Yes"<<endl;
            singleton = new Singleton();//Allocation memory for singleton object.
            singleton->value = "first";
            return singleton;
        }else{
            return singleton;
        }
    }
  
};

Singleton* Singleton::singleton = NULL;

int main()
{
	//Singleton s;
    
    Singleton *s = Singleton::getSingleton();
    cout<<s->value<<endl;
    
    Singleton *s2 = Singleton::getSingleton();
    cout<<s2->value<<endl;
    
	return 0;
}
