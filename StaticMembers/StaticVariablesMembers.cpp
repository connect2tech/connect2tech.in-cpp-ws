
#include<iostream> 
using namespace std; 

/*
void staticVariableFunction(){
    static int count = 0;
    int val = 0;
    
    cout<<"count="<<count<<endl;
    cout<<"val="<<val<<endl;
    
    ++count;
}

int main(){
    staticVariableFunction();
    staticVariableFunction();
    return 0;
}
 */

//--------------------------------------------------


class StaticClassMember 
{ 
public: 
	static int i; 
}; 

int StaticClassMember::i = 10;

int main() 
{ 
    StaticClassMember m1; 
    StaticClassMember m2; 
    m1.i = 100;
    StaticClassMember::i = 200;
    cout<<m1.i<<endl;
    return 0;
}

//-------------------------------------------

/*
class StaticClassFuncation 
{ 
public: 
	static void function1(){
        cout<<"function1"<<endl;
        function2();
        //function3();
    }
    
    static void function2(){
        cout<<"function2"<<endl;
    }
    
    void function3(){
        cout<<"function3"<<endl;
        function1();
    }
}; 



int main() 
{ 
    StaticClassFuncation::function1();
    //StaticClassFuncation::function3();
    StaticClassFuncation obj;
    obj.function3();
    return 0;
}
*/
