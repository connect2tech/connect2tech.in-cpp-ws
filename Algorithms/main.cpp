#include <iostream>   
#include <algorithm>
#include <vector>
using namespace std;

/*
void using_count(){
    int values[] = {5,1,6,5,9,10,1};
    
    int count5 = count(values, values+7, 5);
    cout<<count5<<endl;
    
    vector<int> v(values, values+7);
    
    int count10 = count(v.begin(), v.end(), 10);
    cout<<count10<<endl;

}

int main ()
{
   using_count();
       
    return 0;
}
*/

//-----------------------------------------------------------------

/*
#include <algorithm>
#include <iostream>
 
int main()
{
   int a = 10, b = 20;
 
   std::cout << a << ' ' << b << '\n';
   std::swap(a,b);
   std::cout << a << ' ' << b << '\n';
}*/

//-------------------------------------------------------------------

#include <iostream>
#include <string>
using namespace std;
int main()
   {
  
      // Making use of auto makes it generalized lamba
      auto sum = [](auto a, auto b) {
         return a + b;
         };
       
      cout <<sum(10, 20) << endl; // sum of integers
     
      cout <<sum(21.0, 60.5) << endl; // sum of float numbers
      
      cout <<sum(string("Hello"), string("World")) << endl; // sum of string
    
   
      return 0;
   }
   
   
   