##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Polymorphism
ConfigurationName      :=Debug
WorkspacePath          :=D:/nc/CPP/connect2tech.in-CPP-WS
ProjectPath            :=D:/nc/CPP/connect2tech.in-CPP-WS/Polymorphism
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=naresh
Date                   :=16/05/2020
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=D:/nc/CPP/Installations/mingw64/bin/g++.exe
SharedObjectLinkerName :=D:/nc/CPP/Installations/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Polymorphism.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=D:/nc/CPP/Installations/mingw64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := D:/nc/CPP/Installations/mingw64/bin/ar.exe rcu
CXX      := D:/nc/CPP/Installations/mingw64/bin/g++.exe
CC       := D:/nc/CPP/Installations/mingw64/bin/gcc.exe
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := D:/nc/CPP/Installations/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=$(IntermediateDirectory)/OperatoOverloading.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/OperatoOverloading.cpp$(ObjectSuffix): OperatoOverloading.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/OperatoOverloading.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/OperatoOverloading.cpp$(DependSuffix) -MM OperatoOverloading.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/nc/CPP/connect2tech.in-CPP-WS/Polymorphism/OperatoOverloading.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/OperatoOverloading.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/OperatoOverloading.cpp$(PreprocessSuffix): OperatoOverloading.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/OperatoOverloading.cpp$(PreprocessSuffix) OperatoOverloading.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


