#include <stdio.h>


//Function to find out the maximum of 2 numbers
int max(int x, int y){
    int temp;
    
    if (x>y){
        temp = x;
    }else{
        temp = y;
    }
    
    return temp;
}

int main()
{
    max(10,20);
	printf("hello world\n");
	return 0;
}


//----------------------------------------------------------------------

/*
#include <iostream>
#include <algorithm>
using namespace std;

int square(int x){
    return x*x;
}

int main(){
    int data[5] = {0,1,2,3,4};
    transform(data, data+5, data, square);
    
    for(int i=0;i<5;i++){
        cout<<data[i]<<endl;
    }
}

*/
